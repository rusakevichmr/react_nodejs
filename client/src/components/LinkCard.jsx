import React from "react";

export const LinkCard = ({ link }) => {
  return (
    <div className="font-w5">
      <h2>Link</h2>
      <p>
        Your link:
        <a
          href={link.to}
          rel="noreferrer noopener"
          target="_blank"
          className="ml10"
        >
          {link.to}
        </a>
      </p>
      <p>
        From where:
        <a
          href={link.from}
          rel="noreferrer noopener"
          target="_blank"
          className="ml10"
        >
          {link.from}
        </a>
      </p>
      <p>
        Number of clicks on the link:{" "}
        <strong className="ml10">{link.clicks}</strong>
      </p>
      <p>
        Date create:{" "}
        <strong className="ml10">
          {new Date(link.date).toLocaleDateString()}
        </strong>
      </p>
    </div>
  );
};
