import React, { useState, useEffect, useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { useHttp } from "../hooks/http.hook";
import { useMessage } from "../hooks/message.hook";
export const AuthPage = () => {
  const auth = useContext(AuthContext);
  const message = useMessage();
  const { loading, request, error, clearError } = useHttp();
  const [form, setForm] = useState({ email: "", password: "" });

  useEffect(() => {
    window.M.updateTextFields();
  }, []);

  const changeHandler = (event) => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };
  //обработать массив ошибок
  useEffect(() => {
    message(error);
    clearError();
  }, [error, message, clearError]);

  const registerHandler = async () => {
    try {
      const data = await request("/api/auth/register", "POST", { ...form });
      message(data.message);
    } catch (e) {}
  };

  const loginHandler = async () => {
    try {
      const data = await request("/api/auth/login", "POST", { ...form });
      message(data.message);
      auth.login(data.token, data.userId);
    } catch (e) {}
  };

  return (
    <div className="row">
      <div className="col s6 offset-s3">
        <h1>Сократи ссылку</h1>
        <div className="card blue darken-1">
          <div className="card-image">
            {/* <img src="images/sample-1.jpg" alt="" /> */}
            <span className="card-title"></span>
          </div>
          <div className="card-content white-text">
            <span className="card-title">Authorizations</span>
            <div className="input-field">
              <input
                placeholder="enter email"
                id="email"
                type="email"
                className="validate yellow-input"
                name="email"
                onChange={changeHandler}
                value={form.email}
              />
              <input
                placeholder="enter password"
                id="password"
                type="password"
                className="validate yellow-input"
                name="password"
                onChange={changeHandler}
                value={form.password}
              />
            </div>
          </div>
          <div className="card-action">
            <div
              className="btn yellow darken-4 mr10"
              onClick={loginHandler}
              disabled={loading}
            >
              Sign in
            </div>
            <div
              className="btn yellow darken-4 "
              onClick={registerHandler}
              disabled={loading}
            >
              Registrations
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
