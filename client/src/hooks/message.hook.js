import { useCallback } from "react";

export const useMessage = () => {
  return useCallback((text) => {
    // console.log(window.M, text);
    if (window.M && text) {
      window.M.toast({ html: text });
    }
  }, []);
};
